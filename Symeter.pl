#-----------------------Modify This Section-------------------
#Data file path pointed by Papyrus
my $output_filename
    = 'C:\ISIS\data\SampleData.txt';

#Locate the backup data file. Only use this if you need, Otherwise don't.
my $input_filename
    = 'C:\ISIS\data\ctcu_visa_stmt_060117.split';

#CSV input file for taking multiple account
my $csv_account_list_filename
    = 'C:\Users\anwar\Desktop\UCCU  Monthly Statement Setup\data.csv';

#Account Identifier
my $account_identifier_field = "200";
my $loan_identifier_field = "500";

#-------------------------------------------------------------

print "(1) Find Account Number or Secquence by Line Number\n";
print "(2) Create Data File from Specifiq Account Number\n";
print "(3) Replace Data File with Line Number\n";
print "(4) Count Total Customar\n";
print "(5) Replace Data File with Multiple Account Number\n";
print "(6) Find Account with Multiple Loans\n";
print "\nYour Option: ";

$option_number = <>;
chomp $option_number;

if ( $option_number == 1 ) {
    account_sequence_finder();
}
elsif ( $option_number == 2 ) {
    data_file_replacer();
}
elsif ( $option_number == 3 ) {
    data_file_replacer_with_line_number();
}
elsif ( $option_number == 4 ) {
    total_customer();
}
elsif ( $option_number == 5 ) {
    print " (1) Input account numbers \n";
    print " (2) Read account numbers from file\n";
    print "\nSub Option: ";
    $option_number = <>;
    chomp $option_number;
    if ( $option_number == 1 ) {
        replace_data_file_with_multiple_account();
    }
    elsif ( $option_number == 2 ) {
        replace_data_file_with_multiple_account_from_file();
    }

}
elsif ( $option_number == 5 ) {
    print " (1) Input Single account sequence's range (Ex: 1-5) \n";
    print " (2) Input Multiple account sequence's range (Ex: 1-5) \n";
    print "\nSub Option: ";
    $option_number = <>;
    chomp $option_number;
    if ( $option_number == 1 ) {
        replace_data_file_with_sequence_range();
    }
    elsif ( $option_number == 2 ) {
        replace_data_file_with_multiple_sequence_range();
    }

}elsif ($option_number == 6){
	find_multiple_accounts();
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Anwar
# Purpose 		: Replcae data file with a specific account number
# Parameter 	: null/integer.
# Return Value 	: null
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub data_file_replacer {
    my $line_count           = 0;
    my $account_count        = 0;
    my $account_input_number = 0;
    my @output_account_array;

    if ( @_ != 1 ) {
        print('Enter Account Number: ');
        $account_input_number = <>;
        chomp $account_input_number;
        $account_input_number = $account_input_number * 1;
    }
    else {
        $account_input_number = $_[0];
    }

    my $current_account_number = '';
    my $account_found          = 0;

    #Open File
    open( my $file, '<:encoding(UTF-8)', $input_filename )
        or die "Could not open file '$input_filename' $!";

    while ( my $row = <$file> ) {

        #removing any trailing new lines
        chomp $row;

        #filed code of current line
        my $line_code = substr( $row, 0, 3 );

        #counting line number
        $line_count++;

        #Chekcing if matches the account indetifier field
        # print("$line_code\n");

        if ( $line_code eq $account_identifier_field ) {

            #New account found
            $account_count++;

            my @fields = split /~/, $row;
            $current_account_number = get_field_val( $row, "02" );

        }

        if ( $current_account_number eq $account_input_number ) {
            print("Capturing Line Number: $line_count\n");

            push( @output_account_array, $row );
            $account_found = 1;
        }

        if (   $account_found == 1
            && $current_account_number ne $account_input_number )
        {
            last;
        }

    }

    #Replacing Data File
    open( DATA, ">", $output_filename )
        or die "Couldn't open file $output_filename, $!";

    foreach $row_to_write (@output_account_array) {
        print DATA "$row_to_write\n";
        print "$row_to_write\n";
    }

    print "----------------------------------------------\n";
    print 'Lines replaced: '
        . scalar @output_account_array
        . "                           |\n";
    print "----------------------------------------------\n";

}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Habibur
# Purpose 		: Get a field value from a line
# Parameter 	: String line, field position.
# Return Value 	: return field value, return 0 when field number dows not match.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub get_field_val {
    my ( $row, $fieldpos ) = @_;
    my @fields = split /~/, $row;

    foreach $field (@fields) {
        my $field_code_first = substr $field, 0, 2;
        if ( $field_code_first eq $fieldpos ) {
            $current_account_number = substr $field, 2;
            $current_account_number =~ s/0*(\d+)/$1/;
            return $current_account_number;
        }
    }

    return 0;
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Anwar
# Purpose 		: Find Account Number and Sequence Number from a line number
# Parameter 	: null
# Return Value 	: null
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub account_sequence_finder() {
    my $line_count        = 0;
    my $account_count     = 0;
    my $line_input_number = 0;
    my @output_account_array;

    #Taking input of Account number
    print('Enter Line Number: ');
    $line_input_number = <>;
    chomp $line_input_number;

    my $current_account_number = '';
    my $account_found          = 0;

    #Open File
    open( my $file, '<:encoding(UTF-8)', $input_filename )
        or die "Could not open file '$output_filename' $!";

    while ( my $row = <$file> ) {

        #removing any trailing new lines
        chomp $row;

        #filed code of current line
        my $line_code = substr( $row, 0, 3 );

        #counting line number
        $line_count++;

        #Chekcing if matches the account indetifier field
        # print("$line_code\n");

        if ( $line_code eq $account_identifier_field ) {

            #New account found
            $account_count++;

            my @fields = split /~/, $row;

            foreach $field (@fields) {
                my $field_code_first = substr $field, 0, 2;
                if ( $field_code_first eq '02' ) {
                    $current_account_number = substr $field, 2;
                    $current_account_number =~ s/0*(\d+)/$1/;
                }
            }

            print
                "\tAccount $current_account_number Found at Line $line_count\n";

        }

        if ( $line_count eq $line_input_number ) {
            print("Capturing Line Number: $line_count\n");

            $account_found = 1;
        }

        if ( $account_found == 1 ) {
            last;
        }

    }

    print("Account Found for Line: $current_account_number\n");
    print("Account Secquence Found for Line: $account_count\n");

    return $current_account_number;
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Anwar
# Purpose 		: Replace Data file with a specific line number's Account
# Parameter 	: null
# Return Value 	: null
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub data_file_replacer_with_line_number() {
    my $current_account_number = account_sequence_finder();
    data_file_replacer($current_account_number);
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Anwar
# Purpose 		: Find Total Customer in a file
# Parameter 	: null
# Return Value 	: null
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub total_customer() {
    my $line_count             = 0;
    my $account_count          = 0;
    my $line_input_number      = 0;
    my $current_account_number = '';
    my $account_found          = 0;
    my @output_account_array;

    #Open File
    open( my $file, '<:encoding(UTF-8)', $input_filename )
        or die "Could not open file '$output_filename' $!";

    while ( my $row = <$file> ) {

        #removing any trailing new lines
        chomp $row;

        #filed code of current line
        my $line_code = substr( $row, 0, 3 );

        #counting line number
        $line_count++;

        #Chekcing if matches the account indetifier field
        # print("$line_code\n");

        if ( $line_code eq $account_identifier_field ) {

            #New account found
            $account_count++;
        }

        # print "$account_count\n";
    }

    print "$account_count\n";

    return $account_count;
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Anwar
# Purpose 		: Replace Data file with a Multiple account number
# Parameter 	: null/ref @array
# Return Value 	: null
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub replace_data_file_with_multiple_account {
    my $line_count           = 0;
    my $account_count        = 0;
    my $account_input_number = 0;
    my @input_array;
    my @output_account_array;

    if ( ref( $_[0] ) eq 'ARRAY' ) {
        my ($input_array) = @_;
        @input_array = @{$input_array};
        print @input_array;
    }
    else {
        $account_input_number = 1;

        #Taking Multiple Inputs
        while ($account_input_number) {

            #Taking input of Account number
            print(    'Enter Account Number '
                    . ( ( scalar @input_array ) + 1 )
                    . ': ' );
            $account_input_number = <>;
            chomp $account_input_number;
            if ( $account_input_number eq '' ) {
                last;
            }
            else {
                push( @input_array, $account_input_number );
            }
        }
    }

    if ( scalar @input_array < 1 ) {
        exit;
    }

    my %input_accoutns = map { $_ => 1 } @input_array;

    my $current_account_number = '';
    my $found_account_count    = 0;
    my $found_account_number   = 0;

    #Open File
    open( my $file, '<:encoding(UTF-8)', $input_filename )
        or die "Could not open file '$input_filename' $!";

    while ( my $row = <$file> ) {

        #removing any trailing new lines
        chomp $row;

        #filed code of current line
        my $line_code = substr( $row, 0, 3 );

        #counting line number
        $line_count++;

        #Chekcing if matches the account indetifier field
        # print("$line_code\n");

        if ( $line_code eq $account_identifier_field ) {

            #New account found
            $account_count++;

            my @fields = split /~/, $row;

            foreach $field (@fields) {
                my $field_code_first = substr $field, 0, 2;
                if ( $field_code_first eq '02' ) {
                    $current_account_number = substr $field, 2;
                    $current_account_number =~ s/0*(\d+)/$1/;
                }
            }

            print "\tAccount $current_account_number at Line $line_count \n";

            if ( exists( $input_accoutns{$current_account_number} ) ) {
                $found_account_count++;
                $found_account_number = $current_account_number;
            }
        }

        if ( exists( $input_accoutns{$current_account_number} ) ) {
            print("Capturing Line Number: $line_count\n");

            push( @output_account_array, $row );
        }

        if (   $found_account_count == ( scalar @input_array )
            && $found_account_number ne $current_account_number )
        {
            last;
        }

    }

    #Replacing Data File
    open( DATA, ">", $output_filename )
        or die "Couldn't open file $output_filename, $!";

    foreach $row_to_write (@output_account_array) {
        print DATA "$row_to_write\n";
        print "$row_to_write\n";
    }

    print "----------------------------------------------\n";
    print 'Lines replaced: '
        . scalar @output_account_array
        . "                           |\n";
    print "----------------------------------------------\n";
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Habibur
# Purpose 		: Create data file from multiple account. account number reads from file.
# Parameter 	: null
# Return Value 	: null
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub replace_data_file_with_multiple_account_from_file() {
    my $line_count           = 0;
    my $account_count        = 0;
    my $account_input_number = 0;
    my @input_array;
    my @output_account_array;

    $account_input_number = 1;

    #Replacing Data File
    open( DATA, "<", $csv_account_list_filename )
        or die "Couldn't open file $csv_account_list_filename, $!";

    #Taking Multiple Inputs from file
    while ( my $row = <DATA> ) {

        #Taking account number input of Account number
        @temp_input_array = split /,/, $row;
    }

    # trim all the account number and push to array.
    foreach $i (@temp_input_array) {
        $sliptedvalue = $i;
        $sliptedvalue =~ s/^\s+|\s+$//g;
        push( @input_array, $sliptedvalue );
    }

    print "@input_array \n";

    replace_data_file_with_multiple_account( \@input_array );

}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Author		: Anwar
# Purpose 		: Replcae data file with a specific account number
# Parameter 	: null/integer.
# Return Value 	: null
# not complete
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub data_file_replacer {
    my $line_count           = 0;
    my $account_count        = 0;
    my $account_input_number = 0;
    my @output_account_array;

    if ( @_ != 1 ) {
        print('Enter Account Sequence Range: ');
        $account_input_number = <>;
        chomp $account_input_number;
        $account_input_number = $account_input_number * 1;
    }
    else {
        $account_input_number = $_[0];
    }

    my $current_account_number = '';
    my $account_found          = 0;

    #Open File
    open( my $file, '<:encoding(UTF-8)', $input_filename )
        or die "Could not open file '$input_filename' $!";

    while ( my $row = <$file> ) {

        #removing any trailing new lines
        chomp $row;

        #filed code of current line
        my $line_code = substr( $row, 0, 3 );

        #counting line number
        $line_count++;

        #Chekcing if matches the account indetifier field
        # print("$line_code\n");

        if ( $line_code eq $account_identifier_field ) {

            #New account found
            $account_count++;

            my @fields = split /~/, $row;
            $current_account_number = get_field_val( $row, "02" );

        }

        if ( $current_account_number eq $account_input_number ) {
            print("Capturing Line Number: $line_count\n");

            push( @output_account_array, $row );
            $account_found = 1;
        }

        if (   $account_found == 1
            && $current_account_number ne $account_input_number )
        {
            last;
        }

    }

    #Replacing Data File
    open( DATA, ">", $output_filename )
        or die "Couldn't open file $output_filename, $!";

    foreach $row_to_write (@output_account_array) {
        print DATA "$row_to_write\n";
        print "$row_to_write\n";
    }

    print "----------------------------------------------\n";
    print 'Lines replaced: '
        . scalar @output_account_array
        . "                           |\n";
    print "----------------------------------------------\n";

}


sub find_multiple_accounts {
    my $line_count             = 0;
    my $current_account_number ='';
    my $loan_count = 0; 
    my $account_line_number = 0;

    #Open File
    open( my $file, '<:encoding(UTF-8)', $input_filename )
        or die "Could not open file '$input_filename' $!";

    while ( my $row = <$file> ) {

        #removing any trailing new lines
        chomp $row;

        #filed code of current line
        my $line_code = substr( $row, 0, 3 );


        #counting line number
        $line_count++;

        
        #Chekcing if matches the account indetifier field
        # print("$line_code\n");

        if ( $line_code eq $account_identifier_field ) {
            if($loan_count > 1){
                print "Account Number : $current_account_number\t";
                print "Line Number : $account_line_number\t";
                print "Loan Count : $loan_count\n";
            }
        	$loan_count = 0;
            $account_line_number = $line_count;
        	my @fields = split /~/, $row;

            foreach $field (@fields) {
                my $field_code_first = substr $field, 0, 2;
                if ( $field_code_first eq '02' ) {
                    $current_account_number = substr $field, 2;
                    $current_account_number =~ s/0*(\d+)/$1/;
                }
            }
        }

        if ($line_code == $loan_identifier_field){
        	$loan_count++;
        }

    }

}