import pprint
data = dict()
with open('Book1.csv', 'rU') as f:
	record_number = ''
	field_number = ''
	for line in f:
		record_number = line.split(',')[1].strip()
		field_number = line.split(',')[2].strip()
		if record_number not in data:
			data[record_number] = list()
		if field_number not in data[record_number]:
			data[record_number].append(field_number)


keylist = data.keys()
keylist = sorted(keylist)

for key in keylist:
	temp_string = key+" = '"
	is_first = True
	for item in data[key]:
		if is_first:
			temp_string += item
			is_first = False
		else:
			temp_string += ","
			temp_string += item
	print(temp_string+"'")  

